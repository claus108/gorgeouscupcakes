-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 29, 2022 at 08:55 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ch_gorgeouscupcake`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(50) NOT NULL,
  `category_description` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `category_name`, `category_description`) VALUES
(1, 'Sweet', 'This category includes sweet cupcakes. '),
(2, 'Savoury', 'This category includes savoury cupcakes.'),
(3, 'Special occassion', 'This category includes special occasion cupcakes only available at specific times during the year e.g Easter, Christmas.');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `image_url` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(500) NOT NULL,
  `price` decimal(5,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `category_id`, `image_url`, `name`, `description`, `price`) VALUES
(2, 1, 'cherry-cupcake.jpg', 'Red velvet', 'A delicious dark red cake topped with cream cheese. ', '2.70'),
(3, 2, 'fairy-cake.png', 'Easter nest', 'A fun vanilla Easter themed cupcake topped with an icing nest [April only]. ', '3.10'),
(4, 1, 'rainbow-cupcake.jpg', 'Blueberry', 'A delicious moist cupcake made with fresh blueberries. ', '2.50'),
(5, 2, 'Vanilla-Pink-Cupcake.png', 'Bacon and egg', 'A quirky savoury cupcake for the bacon lovers. ', '3.50'),
(6, 1, 'cherry-cupcake.jpg', 'Coconut chocolate', 'A delightful blend of dark chocolate and coconut. ', '2.50'),
(7, 2, 'fairy-cake.png', 'Corn and spinach', 'A savoury favourite best eaten warm with butter on the side. ', '3.20'),
(8, 3, 'rainbow-cupcake.jpg', 'Christmas tree', 'A vanilla Christmas themed cupcake topped with an icing tree [December only]. ', '3.10'),
(9, 1, 'chocolate-cupcake.jpg', 'White chocolate', 'A delicious white chocolate treat.', '2.50'),
(10, 2, 'Vanilla-Pink-Cupcake.png', 'Bacon and chocolate', 'A unique blend of bacon and chocolate. ', '3.80'),
(14, 1, 'chocolate-cupcake.jpg', 'Chocolate chip', 'A sweet milk chocolate cupcake perfect for all ages.', '2.50');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(256) NOT NULL,
  `password` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `username`, `email`, `password`) VALUES
(14, 'Claus', 'wa@gmx.de', '$2y$10$cvpDDjhjIik/go31St7Ca.s0zf0DyZwK30VQ4Ip8V1HTl2RbNR7/y'),
(76, 'Tom', 'w3@gmx.de', '$2y$10$vboxSpgEWobKmmTVo8wqWeiMHzan2x4HkKerUrTdc6uDh0D5LLuhO');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
