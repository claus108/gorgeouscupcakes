<?php 

function getPDO(){
    $host = 'localhost';
    $db   = 'ch_gorgeouscupcake';
    $user = 'root';
    $pass = '';
    $options = [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION];
    $dsn =  "mysql:host=$host;dbname=$db;ssl-mode=required";
    try {
        $pdo = new PDO($dsn,$user,$pass);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $pdo;
    } catch(PDOException $e) {
        echo "Connection failed: " . $e->getMessage();
    }
}
    
$pdo = getPDO();



function get_categories() {
    global $pdo;
    $sql = "SELECT * from category";
    $stmnt = $pdo->prepare($sql);
    $stmnt->execute();
    $categories = $stmnt->fetchAll(PDO::FETCH_ASSOC);
    
    return $categories;
}

function get_category_name($category_id) {
    global $pdo;
    $sql = "SELECT category_name  FROM category WHERE category_id = :category_id";
    $stmnt = $pdo->prepare($sql);
    $stmnt->bindParam(':category_id', $category_id, PDO::PARAM_INT);
    $stmnt->execute();
    $category = $stmnt->fetch(PDO::FETCH_ASSOC);
    
    return $category['category_name'];
}


function get_products() {
    global $pdo;
    $sql = "SELECT * from product";
    $stmnt = $pdo->prepare($sql);
    $stmnt->execute();
    $products = $stmnt->fetchAll(PDO::FETCH_ASSOC);

    return $products;
}

function get_products_of_category($category_id) {
    global $pdo;
    $sql = "SELECT * from product WHERE category_id = :category_id";
    $stmnt = $pdo->prepare($sql);
    $stmnt->bindParam(':category_id', $category_id);
    $stmnt->execute();
    $products = $stmnt->fetchAll(PDO::FETCH_ASSOC);

    return $products;
}


function get_product($product_id) {
    global $pdo;
    $sql = "SELECT * FROM product WHERE product_id = :product_id";
    $stmnt = $pdo->prepare($sql);
    $stmnt->bindParam(':product_id', $product_id, PDO::PARAM_INT);
    $stmnt->execute();
    $product = $stmnt->fetch(PDO::FETCH_ASSOC);

    return $product;
}


function add_product($category_id, $image_url, $name, $description, $price) {
    global $pdo;
    $sql = "INSERT INTO product (category_id, image_url, name, description, price) VALUES (:category_id, :image_url, :name, :description, :price)";
    $statement = $pdo->prepare($sql);
    $statement->bindValue(':category_id', $category_id);
    $statement->bindValue(':image_url', $image_url);
    $statement->bindValue(':name', $name);
    $statement->bindValue(':description', $description);
    $statement->bindValue(':price', $price);
    $result = $statement->execute();
    $statement->closeCursor();
    return $result;   
}


function update_product($id, $name,$description, $price) {
    global $pdo;
    $sql = "UPDATE product SET name = :productName, description = :productDescription, price = :productPrice WHERE product_id = :productID";
    $statement = $pdo->prepare($sql);
    $statement->bindValue(':productName', $name);
    $statement->bindValue(':productDescription', $description);
    $statement->bindValue(':productPrice', $price);
    $statement->bindValue(':productID', $id);
    $result = $statement->execute();
    $statement->closeCursor();
    return $result;
}

function delete_product($product_id){
    global $pdo;
    $sql = "DELETE FROM product WHERE product_id = :product_id";
    $statement = $pdo->prepare($sql);
    $statement->bindValue(':product_id', $product_id);
    $result = $statement->execute();
    return $result;
}


function signup($uname, $email, $password) {
    global $pdo;
    $sql = "INSERT INTO user (username, email, password) VALUES (:uname, :email, :password)";
    $statement = $pdo->prepare($sql);
    $statement->bindValue(':uname', $uname);
    $statement->bindValue(':email', $email);
    $statement->bindValue(':password', $password);
    $result = $statement->execute();
    $statement->closeCursor();
    return $result;  
}

function login($email, $password) {
    global $pdo;
    $sql = "SELECT * FROM user WHERE email = :email";
    $stmnt = $pdo->prepare($sql);
    $stmnt->bindParam(':email', $email);
    $stmnt->execute();
    $user = $stmnt->fetch(PDO::FETCH_ASSOC);
    // verify password and email on login
    if(password_verify($password, $user['password']) && $user['email'] === $email){
        session_start();
        $_SESSION['user'] = $user['username'];
        header('location:index.php');
    } else {
        header('location:index.php?p=login_error');
    }
    
}

?>