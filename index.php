<?php

    session_start();

    include 'model/database.php';
    include('controller/products_fetch_process.php');

    //Validate which page to show
    if(isset($_GET['p'])) {
        $p = $_GET['p'];
    } elseif (isset($_POST['p'])) {
        $p = $_POST['p'];
    } else {
        $p = NULL;
    }

    $page = 'main.php';
    $page_title = "Home | Gorgeous Cupcakes"; 
    $category_name = "";
    $product = [];
    $login_error = false;
    $add_error = false;
    $update_error = false;
    $required_error = false;
    $numeric_error = false;
    $signup_error = false;

    //Determine, which page page to show
    switch ($p) {
        case 'category':
            $category_id = $_GET['cid'];
            $category_name = get_category_name($category_id);
            include 'controller/products_of_categories.php';
            $page_title = $category_name . "| Gorgeous Cupcakes";
            break;
        case 'signup_error':
            $signup_error = true;
        case 'signup':
            $page = 'signup.php';
            $page_title = 'Sign up';
            break;
        case 'signup_process';
            include('controller/signup_process.php');
            // include('controller/products_fetch_process.php');
            break;
        case 'login_error':
            $login_error = true;
        case 'login' :
            $page = 'login.php';
            $page_title = 'Log In';
            break;
        case 'login_process':
            include('controller/login_process.php');
            // include('controller/products_fetch_process.php');
            break;
        case 'logout':
            include('controller/logout_process.php');
            // include('controller/products_fetch_process.php');
            break;
        case 'add_error':
            $error_type = $_GET['error_type'];
            if($error_type === 'required'){
                echo "add error";
                $required_error = true;
            }
            if($error_type === 'not_numeric'){
                $numeric_error = true;
            }
        case 'add_product':
            if($_SESSION){
                include('controller/categories_fetch_process.php');
                $page = 'add_product.php';
                $page_title = "Add product | Gorgeous Cupcakes";
            }
            break;
        case 'add_process':
            if($_SESSION) {
                include('controller/product_add_process.php');
                // include 'controller/products_fetch_process.php';
            }
            break;
        case 'update_error':
            $error_type = $_GET['error_type'];
            if($error_type === 'required'){
                $required_error = true;
            }
            if($error_type === 'not_numeric'){
                $numeric_error = true;
            }
        case 'update':
            if($_SESSION) {
                $productID = $_GET['productID'];
                include('controller/product_fetch_process.php');
                $page = 'update.php';
                $page_title = "Update";
            }
            break;
        case 'update_process':
            if($_SESSION) {
                include('controller/product_update_process.php');
            }
            break;
        case 'delete':
            if($_SESSION) {
                $productID = $_GET['productID'];
                include('controller/product_delete_process.php');
                // include 'controller/products_fetch_process.php';
            }
            break;
       
        default:
            include 'controller/products_fetch_process.php';
            $page = 'main.php';
            $page_title = "Home | Gorgeous Cupcakes"; 
    }

    // make sure the choosen file actually exists
    if(!file_exists('./view/' . $page)) {
        $page = 'main.php';
        $page_title = "Home | Gorgeous Cupcakes";
    }

    // include the header
    include ('view/header.php');

    // include the content according to switch statement outcome.
    include('./view/' . $page);

    // include the footer
    include ('view/footer.php');

?>

