
<div class="container">
    <div class='col-md-4'> 
        <div class='form-box'>
            <?php 
                if($required_error) { // display message in case one of the required fields is empty
                    echo "<div class='form-error'>All * fields are required</div>";
                }
                if($numeric_error) { // display message in case price is not numeric
                    echo "<div class='form-error'>Price has to be a numeric value</div>";
                }
            ?>
            <h1>Add a new product</h1>
            <form enctype="multipart/form-data" action="index.php" method="POST">
                <input type='hidden' name='p' value='add_process'>
                <div class="form-group">
                    <label for='name'>Name *</label><br>
                    <input type='text' class="form-control" id='name' name='name'><br>
                </div>
                <div class="form-group">
                    <label for="categories">Category</label>
                    <select class="form-control" name="categories" id="categories">
                    <option value="">--- Choose a category ---</option>
                        <?php 
                            foreach($categories as $cat):
                                echo "<option value='" . $cat['category_id'] . "'>" . $cat['category_name'] . "</option>";
                            endforeach;
                        ?>
                    </select><br>
                </div>
                <div class="form-group">
                    <label for="fileToUpload">Upload Image</label>
                    <input type="file" class="form-control" name="fileToUpload" id="fileToUpload">
                </div>
                <div class="form-group">
                    <label for='description'>Description *</label><br>
                    <textarea class="form-control" id='description' name='description' rows='3'></textarea><br>
                </div>
                <div class="form-group">
                    <label for='price'>Price *</label><br>
                    <input class="form-control" type='text' id='price' name='price'><br>
                </div>
                <br><br>
                <input class="btn btn-primary" type='submit' value='Add Product'>
            </form> 
        </div>
    </div>
</div>

