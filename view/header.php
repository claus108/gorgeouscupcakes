<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="view/css/styles.css">
    <title><?php echo $page_title; ?></title>
</head>

<body>
    <header>
        <nav>
            <a id="navButton"><span class="fas fa-bars fa-3x"></span></a>
            <ul id="navList" class="navHide">
                <li><a class="current-link" href="index.php">Home</a></li>
                <li><a href="index.php?p=about">About</a></li>
                <?php 
                    // only show add and log out links when login session is active
                    if($_SESSION) {
                        echo "<li><a href='index.php?p=add_product'>Add Product</a></li>";
                        echo "<li><a class='btn btn-primary btn-sm logout-btn' href='index.php?p=logout'>Log Out</a></li>";
                    } else {
                        echo "<li><a href='index.php?p=signup'>Sign Up</a></li>";
                        echo "<li><a href='index.php?p=login'>Log In</a></li>";

                    }
                ?>
                <?php 
                    if (isset($_SESSION['user'])) {
                        echo '<li>Welcome ' . $_SESSION['user'] . '</li>';
                    }
                ?>
            </ul>
        </nav>
        <div id="title-box">
            <h1 id="title">Gorgeous Cupcakes</h1>
            <div id="sub-title">- Bakery -</div>
        </div>
    </header>
 