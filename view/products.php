<?php

    

    foreach($products as $product):
        displayProduct($product['name'], $product['product_id'], $product['image_url'], $product['description'], $product['price']);
    endforeach;

    function displayProduct($name, $id, $image, $description, $price) {
        $str =
            "<div class='col-md-4'>" . 
            "<div class='product-box'>" .
            "   <h3>$name</h3>" .
            "   <img src='view/images/$image' alt='$name'>".
            "   <p>$description</p>" .
            "   <p>$ ". number_format($price,2) . "</p>";
        // Update and delete links are only available when login session is active
        if($_SESSION) {
            $str = $str .
            "   <a href='index.php?p=update&productID=$id' class='btn btn-primary btn-sm'>Update</a>" .
            "   <a href='index.php?p=delete&productID=$id' class='btn btn-danger btn-sm'>Delete</a>";
        }
        $str = $str .
            "</div>" .
            "</div>";

        echo $str;
    }

?>
