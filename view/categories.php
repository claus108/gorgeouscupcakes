<?php 

    require 'controller/categories_fetch_process.php';

    foreach($categories as $category):
        displayCategory($category);
    endforeach;

    function displayCategory($category) {
        echo "<p><a href='index.php?p=category&cid=" .
            $category['category_id'] ."'>" .
            $category['category_name'] . "</a></p>";
    }

?>