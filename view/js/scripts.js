// required, password > 8 chars, email invalid


var form = document.getElementById('signupform');
const username = document.getElementById('uname');
const email = document.getElementById('email');
const password = document.getElementById('password');
const password2 = document.getElementById('password2');


form.addEventListener('submit', e => {
    if(!checkInputs()) {
        e.preventDefault();
        console.log('ho');
    }
});


function checkInputs() {
    // trim to remove the whitespaces
    const unameValue = uname.value.trim();
    const emailValue = email.value.trim();
    const passwordValue = password.value.trim();
    const password2Value = password2.value.trim();

    var isValid = true;

    if (unameValue === "") {
        console.log('uname');
        errorMessage(uname, "Username is required");
        isValid = false;
    } else {
        setSuccess(uname);
    }

    if (emailValue === "") {
        errorMessage(email, 'Email is required');
        isValid = false;
    } else if (!isEmail(emailValue)) {
        errorMessage(email, "Please enter a valid email");
        isValid = false;
    } else {
        setSuccess(email);
    }

    if (passwordValue === "") {
        errorMessage(password, 'Password is required');
        isValid = false;
    } else if (passwordValue.length < 8) {
        errorMessage(password, 'Password needs at least 8 characters');
        isValid = false;
    } else {
        setSuccess(password);
    }

    if (password2Value === "") {
        errorMessage(password2, "Please check your password");
        isValid = false;
    } else if (passwordValue !== password2Value) {
        errorMessage(password2, "Passwords don't match");
        isValid = false;
    } else {
        setSuccess(password2);
    }

    return isValid;
}

function isEmail(email) {
    return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
}


function errorMessage(input, message) {
    const formControl = input.parentElement;
    const small = formControl.querySelector('small');
    formControl.className = 'form-group error';
    small.innerText = message;
}

function setSuccess(input) {
    const formControl = input.parentElement;
    const small = formControl.querySelector('small');
    formControl.className = '';
    small.style.visibility = "hidden";
}