
<section class="main">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <h2>
                    <?php
                        if($category_name) {
                            echo $category_name;
                        } 
                    ?>
                    Cupcakes
                </h2>
                <div class="row product-row">
                    <?php include 'products.php'; ?>
                <!-- END PRODUCT-ROW -->
                </div>
            <!-- END COLUMN-9  -->
            </div>
            <div class="col-md-3 categories-box">
                <h3>Categories</h3>
                <?php include 'categories.php'; ?>
            </div>
        </div>
    </div>
</section>