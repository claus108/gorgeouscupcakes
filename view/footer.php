<footer>
   
        <a href="index.php">Home</a>
        <a href="index.php?p=about">About</a>
        <a href="index.php?p=feedback">Feedback</a>
        <a href="index.php?p=contact">Contact Us</a>
        <address>
            40 Mills Street
            Brisbane City
            Queensland 4000
            <div>
                Claus Hennig &copy; 
                <?php 
                    // use the date(format, timestamp) function for displaying the current year
                    // with format 'Y' representing a four digit year.
                    echo date('Y'); 
                ?>
            </div>
        </address>
    </footer>

    <script>
        let list = document.getElementById('navList');
        let button = document.getElementById('navButton');

        button.addEventListener('click', () => {
            list.classList.contains('navHide') ? list.classList.remove('navHide') : list.classList.add('navHide');
        });
        list.onmouseleave = () => {
            list.classList.add('navHide');
        };
    </script>
    <script src="view/js/scripts.js"></script>
</body>

</html>