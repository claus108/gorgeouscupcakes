
<div class="container">
    <div class='col-md-4'> 
        <div class='form-box'>
        <?php 
                if($signup_error) {
                    echo "<div class='form-error'>Please enter a valid email</div>";
                }
            ?>
            <h1>Sign Up</h1>
            <form id="signupform" action="index.php" method="POST">
                <input type='hidden' name='p' value='signup_process'>
                <div class="form-group">
                    <label for='uname'>Username</label>
                    <!-- input field for username -->
                    <input type='text' class="form-control" id='uname' name='uname'><br>
                    <!-- hidden field for error message -->
                    <small></small>
                </div>
                <div class="form-group">
                    <label for='email'>Email</label>
                    <!-- input field for email address -->
                    <input type="text" class="form-control" id='email' name='email'><br>
                    <!-- hidden field for error message -->
                    <small></small>
                </div>
                <div class="form-group">
                    <label for='password'>Password</label>
                    <!-- input field for the password -->
                    <input class="form-control" type='password' id='password' name='password'><br>
                    <!-- hidden field for error message -->
                    <small></small>
                </div>
                <div class="form-group">
                    <label for='password2'>Password Check</label>
                    <!-- input field for the password check -->
                    <input class="form-control" type='password' id='password2' name='password2'><br>
                    <!-- hidden field for error message -->
                    <small></small>
                </div>
                <br><br>
                <input class="btn btn-primary" type='submit' value='Sign Up'>
            </form> 
        </div>
    </div>
</div>

