<?php

        $name = $product['name'];
        $description = $product['description'];
        $price = $product['price'];
        $id = $product['product_id'];

        echo "<div class='container'>" .
            "<div class='col-md-4'> " .
                "<div class='form-box'>";
                if($required_error) {
                    echo "<div class='form-error'>All * fields are required</div>";
                }
                if($numeric_error) {
                    echo "<div class='form-error'>Price has to be a numeric value</div>";
                }
                echo "<h1>Update Product: $name</h1>" .
                    "<form action='index.php' method='POST'>" .
                        "<input type='hidden' name='p' value='update_process'>" .
                        "<input type='hidden' name='id' value='$id'>" .
                        "<div class='form-group'>" .
                            "<label for='name'>Name:</label><br>" .
                            "<input class='form-control' type='text' id='name' name='name' value='$name'><br>" .
                        "</div>" .
                        "<div class='form-group'>" .
                            "<label for='description'>Description</label><br>" .
                            "<textarea class='form-control' id='description' name='description' rows='3'>$description</textarea><br>" .
                        "</div>" .
                        "<div class='form-group'>" .
                            "<label for='price'>Price</label><br>" .
                            "<input class='form-control' type='text' id='price' name='price' value='$price'><br>" .
                        "</div>" .
                        "<br><br>" .
                        "<input class='btn btn-primary' type='submit' value='Submit Update'>" .
                    "</form> " .
                "</div>" .
            "</div>" .
        "</div>";


?>