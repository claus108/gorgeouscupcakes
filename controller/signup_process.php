<?php

    $uname = $_POST['uname'];
    $email = $_POST['email'];

    // check if the posted email value is valid
    if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
        // hash the submited password for inserting into the database
        $password = password_hash($_POST['password'], PASSWORD_DEFAULT);
        // insert submitted data into the database
        signup($uname, $email, $password);
    } else {
        // relocate back to index.php in case the email is not valid
        header('location:index.php?p=signup_error');
    }



?>