<?php

  // assign all posted form values to variables
  $name = $_POST['name'];
  $category_id = $_POST['categories'];
  $description = $_POST['description'];
  $price = $_POST['price'];
  $image_url = basename($_FILES["fileToUpload"]["name"]);
  
  
  if(empty($name) || empty($description) || empty($price)) { // check if required fields are empty
    // relocate back to index with add_error and required 
    // for displaying the conditional error message
    header('location:index.php?p=add_error&error_type=required');
  } else if(!is_numeric($price)) {  // check if the posted price is numeric
    // relocate back to index with add_error and not_numeric
    // for displaying the conditional error message
    header('location:index.php?p=add_error&error_type=not_numeric');
  } else {
    // insert data into the database
    add_product($category_id, $image_url, $name, $description, $price);
    upload_image($image_url);
  }



  function upload_image($image_url) {

    $target_dir = "view/images/";
    $target_file = $target_dir . $image_url;
    
    move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file);

  }
?>