<?php
    $id = $_POST['id'];
    // check if any of the posted values are empty
    if(empty($_POST['name']) || empty($_POST['description']) || empty($_POST['price'])) {
        // redirect to index.php including the error_type required for displaying the error message
        header('location:index.php?p=update_error&error_type=required&productID=' . $id);
    }

    $name = $_POST['name'];
    $description = $_POST['description'];
    $price = $_POST['price'];

    // check if the posted price value is numeric
    if(!is_numeric($price)) {
        // redirect back to index in case with error_type not_numeric 
        header('location:index.php?p=update_error&error_type=not_numeric&productID=' . $id);
    }
    

    update_product($id, $name, $description, number_format($price,2));

?>